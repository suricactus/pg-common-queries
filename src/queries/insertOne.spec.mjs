import insertOneQuerier from './insertOne';

describe('QUERY insertOne', () => {
  it('should generate sql and parameter for inserting one row', () => {
    const insertOneQuery = insertOneQuerier({
      table: 'table',
      writableCols: ['columna', 'columnb'],
    });
    expect(insertOneQuery({ columna: 'a', columnb: 'b' })).toEqual({
      sql:
        'INSERT INTO table (columna, columnb) VALUES ($<columna>, $<columnb>) RETURNING *',
      parameters: {
        columna: 'a',
        columnb: 'b',
      },
      returnOne: true,
    });
  });

  it('should ignore parameter not in column', () => {
    const insertOneQuery = insertOneQuerier({
      table: 'table',
      writableCols: ['columna', 'columnb'],
    });
    expect(
      insertOneQuery({ columna: 'a', columnb: 'b', columnc: 'ignored' })
    ).toEqual({
      sql:
        'INSERT INTO table (columna, columnb) VALUES ($<columna>, $<columnb>) RETURNING *',
      parameters: {
        columna: 'a',
        columnb: 'b',
      },
      returnOne: true,
    });
  });

  it('should ignore missing parameter', () => {
    const insertOneQuery = insertOneQuerier({
      table: 'table',
      writableCols: ['columna', 'columnb'],
    });
    expect(insertOneQuery({ columna: 'a' })).toEqual({
      sql: 'INSERT INTO table (columna) VALUES ($<columna>) RETURNING *',
      parameters: {
        columna: 'a',
      },
      returnOne: true,
    });
  });

  it('should ignore missing writableCols', () => {
    const insertOneQuery = insertOneQuerier({
      table: 'table',
    });
    expect(insertOneQuery({ columna: 'a' })).toEqual({
      sql: 'INSERT INTO table (columna) VALUES ($<columna>) RETURNING *',
      parameters: {
        columna: 'a',
      },
      returnOne: true,
    });
  });
});
