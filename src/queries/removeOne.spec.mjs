import removeOne from './removeOne';

describe('QUERY removeOne', () => {
  it('Should delete without specifying the primaryKey column name', () => {
    const removeOneQuery = removeOne({
      table: 'table',
      primaryKey: ['id_col'],
      returnCols: ['columna', 'columnb'],
    });

    expect(removeOneQuery(100)).toEqual({
      sql: 'DELETE FROM table id_col = $<id_col> RETURNING columna, columnb',
      parameters: {
        id_col: 100,
      },
      returnOne: true,
    });
  });

  it('Should return all columns', () => {
    const removeOneQuery = removeOne({
      table: 'table',
      primaryKey: ['id1', 'id2'],
    });
    expect(removeOneQuery({ id1: 1, id2: 2 })).toEqual({
      sql:
        'DELETE FROM table WHERE id1 = $<id1> AND id2 = $<id2> RETURNING *',
      parameters: {
        id1: 1,
        id2: 2,
      },
      returnOne: true,
    });
  });

  it('should generate sql and parameter for selecting one row', () => {
    const removeOneQuery = removeOne({
      table: 'table',
      primaryKey: ['id1', 'id2'],
      returnCols: ['columna', 'columnb'],
    });
    expect(removeOneQuery({ id1: 1, id2: 2 })).toEqual({
      sql:
        'DELETE FROM table WHERE id1 = $<id1> AND id2 = $<id2> RETURNING columna, columnb',
      parameters: {
        id1: 1,
        id2: 2,
      },
      returnOne: true,
    });
  });

  it('should ignore parameters not in selectors', () => {
    const removeOneQuery = removeOne({
      table: 'table',
      primaryKey: ['id1', 'id2'],
      returnCols: ['*'],
    });
    expect(
      removeOneQuery({ id1: 1, id2: 2, columna: 'a', columnb: 'b' })
    ).toEqual({
      sql: 'DELETE FROM table WHERE id1 = $<id1> AND id2 = $<id2> RETURNING *',
      parameters: {
        id1: 1,
        id2: 2,
      },
      returnOne: true,
    });
  });

  it('should apply permanent filters', () => {
    const removeOneQuery = removeOne({
      table: 'table',
      primaryKey: ['id1', 'id2'],
      returnCols: ['columna', 'columnb'],
      permanentFilters: { columnc: 'foo' },
    });
    expect(removeOneQuery({ id1: 1, id2: 2 })).toEqual({
      sql:
        'DELETE FROM table WHERE id1 = $<id1> AND id2 = $<id2> AND columnc = $<columnc> RETURNING columna, columnb',
      parameters: {
        id1: 1,
        id2: 2,
        columnc: 'foo',
      },
      returnOne: true,
    });
  });
});
