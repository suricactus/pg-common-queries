import selectOneQuerier from './selectOne';

describe('QUERY selectOne', () => {
  it('should generate sql and parameter for selecting one row', () => {
    const selectOneQuery = selectOneQuerier({
      table: 'table',
      primaryKey: ['id1', 'id2'],
      returnCols: ['columna', 'columnb'],
    });
    expect(selectOneQuery({ id1: 1, id2: 2 })).toEqual({
      sql:
        'SELECT columna, columnb FROM table WHERE id1 = $<id1> AND id2 = $<id2> LIMIT 1',
      parameters: {
        id1: 1,
        id2: 2,
      },
      returnOne: true,
    });
  });

  it('should generate sql and params to select one row when receiving single value', () => {
    const selectOneQuery = selectOneQuerier({
      table: 'bib_user',
      primaryKey: 'username',
      returnCols: ['*'],
    });
    expect(selectOneQuery('john')).toEqual({
      sql: 'SELECT * FROM bib_user WHERE username = $<username> LIMIT 1',
      parameters: {
        username: 'john',
      },
      returnOne: true,
    });
  });

  it('should ignore parameters not in primaryKey', () => {
    const selectOneQuery = selectOneQuerier({
      table: 'table',
      primaryKey: ['id1', 'id2'],
      returnCols: ['columna', 'columnb'],
    });
    expect(
      selectOneQuery({ id1: 1, id2: 2, columna: 'a', columnb: 'b' })
    ).toEqual({
      sql:
        'SELECT columna, columnb FROM table WHERE id1 = $<id1> AND id2 = $<id2> LIMIT 1',
      parameters: {
        id1: 1,
        id2: 2,
      },
      returnOne: true,
    });
  });

  it('should generate sql and parameter with permanent filters', () => {
    const selectOneQuery = selectOneQuerier({
      table: 'table',
      primaryKey: ['id1'],
      returnCols: ['columna', 'columnb'],
      permanentFilters: { foo: 'bar', bar: 'IS NULL', tor: 'IS NOT NULL' },
    });
    expect(selectOneQuery({ id1: 1 })).toEqual({
      sql:
        'SELECT columna, columnb FROM table WHERE id1 = $<id1> AND foo = $<foo> AND bar IS NULL AND tor IS NOT NULL LIMIT 1',
      parameters: {
        bar: 'IS NULL',
        foo: 'bar',
        id1: 1,
        tor: 'IS NOT NULL',
      },
      returnOne: true,
    });
  });
});
