import batchInsertQuerier from './batchInsert';

describe('QUERY batchInsert', () => {
  it('should generate sql and parameter for batchInserting given rows', () => {
    const batchInsertQuery = batchInsertQuerier({
      table: 'table',
      writableCols: ['columna', 'columnb'],
    });
    expect(
      batchInsertQuery([{ columna: 1, columnb: 2 }, { columna: 3, columnb: 4 }])
    ).toEqual({
      sql:
        'INSERT INTO table(columna, columnb) VALUES ($<columna1>, $<columnb1>), ($<columna2>, $<columnb2>) RETURNING *',
      parameters: {
        columna1: 1,
        columnb1: 2,
        columna2: 3,
        columnb2: 4,
      },
    });
  });

  it('should set to null if column is missing', () => {
    const batchInsertQuery = batchInsertQuerier({
      table: 'table',
      writableCols: ['columna', 'columnb'],
    });
    expect(
      batchInsertQuery([{ columna: 1 }, { columna: 3, columnb: 4 }])
    ).toEqual({
      sql:
        'INSERT INTO table(columna, columnb) VALUES ($<columna1>, $<columnb1>), ($<columna2>, $<columnb2>) RETURNING *',
      parameters: {
        columna1: 1,
        columnb1: null,
        columna2: 3,
        columnb2: 4,
      },
    });
  });

  it('should generate correct query with undefined `writableCols`', () => {
    const batchInsertQuery = batchInsertQuerier({
      table: 'table',
    });
    expect(
      batchInsertQuery([{ columna: 1, columnb: 2 }, { columna: 3 }])
    ).toEqual({
      sql:
        'INSERT INTO table(columna, columnb) VALUES ($<columna1>, $<columnb1>), ($<columna2>, $<columnb2>) RETURNING *',
      parameters: {
        columna1: 1,
        columnb1: 2,
        columna2: 3,
        columnb2: null,
      },
    });
  });
});
