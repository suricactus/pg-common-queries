import namedParameter from './namedParameter';
import curry from '../../utils/curry';

function valueSubQuery (writableCols, suffix) {
  return writableCols.map(column => namedParameter(`${column}${suffix}`)).join(', ');
}

export default curry(valueSubQuery);
