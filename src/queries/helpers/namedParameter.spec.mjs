import namedParameter from './namedParameter';

test('Create named parameter', () => {
  expect(namedParameter('alibaba')).toEqual('$<alibaba>');
});
