import upsertOneQuerier from './upsertOne';

describe('QUERY upsertOne', () => {
  it('should generate sql and parameter for upserting one row', () => {
    const upsertOneQuery = upsertOneQuerier({
      table: 'table',
      primaryKey: ['id1', 'id2'],
      writableCols: ['columna', 'columnb'],
    });
    expect(
      upsertOneQuery({ id1: 1, id2: 2, columna: 'a', columnb: 'b' })
    ).toEqual({
      sql: `INSERT INTO table (id1, id2, columna, columnb)
VALUES ($<id1>, $<id2>, $<columna>, $<columnb>)
ON CONFLICT (id1, id2)
DO UPDATE SET columna = $<columna>, columnb = $<columnb>
RETURNING *`,
      parameters: {
        id1: 1,
        id2: 2,
        columna: 'a',
        columnb: 'b',
      },
      returnOne: true,
    });
  });

  it('should generate query for upserting using same order for (column...) and VALUES(column...)', () => {
    const upsertOneQuery = upsertOneQuerier({
      table: 'table',
      primaryKey: ['id'],
      writableCols: ['column'],
    });
    expect(upsertOneQuery({ column: 'value', id: 1 })).toEqual({
      sql: `INSERT INTO table (id, column)
VALUES ($<id>, $<column>)
ON CONFLICT (id)
DO UPDATE SET column = $<column>
RETURNING *`,
      parameters: {
        id: 1,
        column: 'value',
      },
      returnOne: true,
    });
  });

  it('should not try to update column not passed in row', () => {
    const upsertOneQuery = upsertOneQuerier({
      table: 'table',
      primaryKey: ['id'],
      writableCols: ['columna', 'columnb'],
    });
    expect(upsertOneQuery({ columna: 'value', id: 1 })).toEqual({
      sql: `INSERT INTO table (id, columna)
VALUES ($<id>, $<columna>)
ON CONFLICT (id)
DO UPDATE SET columna = $<columna>
RETURNING *`,
      parameters: {
        id: 1,
        columna: 'value',
      },
      returnOne: true,
    });
  });

  it('should DO NOTHING on conflict when no value provided to updatable column', () => {
    const upsertOneQuery = upsertOneQuerier({
      table: 'table',
      primaryKey: ['id'],
      writableCols: ['column'],
    });
    expect(upsertOneQuery({ id: 1 })).toEqual({
      sql: `INSERT INTO table (id)
VALUES ($<id>)
ON CONFLICT (id)
DO NOTHING
RETURNING *`,
      parameters: {
        id: 1,
      },
      returnOne: true,
    });
  });

  it('should accept to have empty writableCols', () => {
    const upsertOneQuery = upsertOneQuerier({
      table: 'table',
      primaryKey: ['id'],
      writableCols: [],
    });
    expect(upsertOneQuery({ id: 1, name: 'value' })).toEqual({
      sql: `INSERT INTO table (id)
VALUES ($<id>)
ON CONFLICT (id)
DO NOTHING
RETURNING *`,
      parameters: {
        id: 1,
      },
      returnOne: true,
    });
  });

  it('should accept to have undefined writableCols', () => {
    const upsertOneQuery = upsertOneQuerier({
      table: 'table',
      primaryKey: ['id'],
    });
    expect(upsertOneQuery({ id: 1, name: 'value' })).toEqual({
      sql: `INSERT INTO table (id, name)
VALUES ($<id>, $<name>)
ON CONFLICT (id)
DO UPDATE SET id = $<id>, name = $<name>
RETURNING *`,
      parameters: {
        id: 1,
        name: 'value',
      },
      returnOne: true,
    });
  });

  it('should apply permanent filters', () => {
    const upsertOneQuery = upsertOneQuerier({
      table: 'table',
      primaryKey: ['id'],
      writableCols: ['column'],
      permanentFilters: { columnb: 'foo' },
    });
    expect(upsertOneQuery({ column: 'value', id: 1 })).toEqual({
      sql: `INSERT INTO table (id, column)
VALUES ($<id>, $<column>)
ON CONFLICT (id)
DO UPDATE SET column = $<column> WHERE columnb = $<columnb>
RETURNING *`,
      parameters: {
        id: 1,
        column: 'value',
        columnb: 'foo',
      },
      returnOne: true,
    });
  });
});
