# coPostgresQueries

Utility to generate and execute postgresql queries with ease.

## Install

`npm install --save pg-common-queries`

## Introduction

The [querybuilders](#query-builder) (insertOne, selectOne, etc..) that allows to generate sql, and the corresponding parameters.

Each query builder takes the form:

```js
query(config)(...parameters);
```

On the first call it receives its configuration, eg, the table name, column name, etc...
For example:

```js
import insertOne  from 'pg-common-queries/queries/insertOne';
const insertOne = insertOne({
    table: 'user',
    writableCols: ['name', 'firstname'],
    returnCols: ['id', 'name', 'firstname'],
});
```

On the second call it takes the query parameters and returns an object of the form `{ text, parameters }`,

- text: the sql string that may contain named parameters
- parameters: the sanitized named parameters and their values.

For example:

```js
insertOne({ name: 'doe', firstname: 'john', other: 'data' });
// would return
{
    text: 'INSERT INTO user (name, firstname)VALUES($name, $firstname) RETURNING id, name, firstname',
    parameters: { name: 'doe', firstname: 'john' }
}
```

The result can then be directly passed to [`client.query`](#clientquery) to be executed.

```js
client.query(insertOne({ name: 'doe', firstname: 'john', other: 'data' }));
```

## Api

### Query builder

Each query helper takes the form:

```js
query(config)(...parameters);
```

On the first call it receives its configuration, eg, the table name, column name, etc...
For example:

```js
import insertOne  from 'pg-common-queries/queries/insertOne';
const insertOne = insertOne({
    table: 'user',
    writableCols: ['name', 'firstname'],
    returnCols: ['id', 'name', 'firstname'],
});
```

On the second call it takes the query parameters and returns an object of the form `{ sql, parameters }`,
with the sql containing named parameter, and parameters having been sanitized based on the configuration.
For example:

```js
insertOne({ name: 'doe', firstname: 'john', other: 'data' });
// would return
{
    text: 'INSERT INTO user (name, firstname)VALUES($name, $firstname) RETURNING id, name, firstname',
    parameters: { name: 'doe', firstname: 'john' }
}
```

The result can then be directly passed to `client.query` to be executed.

#### insertOne

```js
import insertOne  from 'pg-common-queries/queries/insertOne';
insertOne({ table, writableCols, returnCols })(row)
```

Returns a query to insert one given row.

##### Configuration

- table: the table name
- writableCols: lisft of columns that can be set
- returnCols: list of columns exposed in the result of the query

##### Parameters

A literal object in the form of:

```js
{
    column: value,
    ...
}
```

#### batchInsert(table, writableCols, returnCols)(rows)

```js
import batchInsert from 'pg-common-queries/queries/batchInsert';
batchInsert(table, writableCols, returnCols)(rows);
```

allow to create a query to insert an array of rows.

##### Configuration

- table: the table name
- writableCols: list of columns that can be set
- returnCols: list of columns exposed in the result of the query

##### Parameters

An array of literal objects in the form of:

```js
[
    {
        column: value,
        ...
    }, ...
]
```

#### selectOne

```js
import selectOne  from 'pg-common-queries/queries/selectOne';
selectOne({ table, primaryKey, returnCols, permanentFilters })(row)
```

Creates a query to select one row.

##### Configuration

- table: the table name
- primaryKey: One or more columns representing the primary key. Accept either an array or a single value. (default: `id`)
- returnCols: list of columns retrieved by the query
- permanentFilters: List of filters applied by default, e. g. for a soft delete with permanentFilters as `{ deleted_at: null}`

##### Parameters

A literal in the form of:

```js
{
    id1: value,
    id2: value,
    ...
}
```

Any key not present in primaryKey will be ignored.

#### select

```js
import select from 'pg-common-queries/queries/select';
select({
    table,
    primaryKey,
    returnCols,
    searchableCols,
    specificSorts,
    groupByCols,
    withQuery,
    permanentFilters,
    returnOne,
})({ limit, offset, filters, sort, sortDir });
```

Creates a query to select one row.

##### Configuration

- table:
    the table name, accept JOIN statements
- primaryKey: One or more columns representing the primary key. Accept either an array or a single value. (default: `id`)
- returnCols:
    list of columns retrieved by the query
- searchableCols:
    list of columns that can be searched (usable in filter parameter). Defaults to return columns
- specificSorts:
    allow to specify sort order for a given column. Useful when we want to order string other than by alphabetical order.
    example:
    ```js
    {
        level: ['master', 'expert', 'novice']
    }
    ```
    will order level column with all master first, then expert and finally novice
- groupByCols
    allow to add a GROUP BY clause to the query on the given columns
- withQuery
    specify that we want to encompass the query in `WITH RESULT AS <query> SELECT * FROM result`
    This add a temporary result table that allow to sort on computed and joined column.
    if the table configuration contain a JOIN clause, this will be automatically set to true.
- permanentFilters: List of filters applied by default, e. g. for a soft delete with permanentFilters as `{ deleted_at: null}`
- returnOne: Optional, if set to true, returns only the first result instead of an array.

##### Parameters

A literal object with:

- limit:
    number of results to be returned
- offset:
    number of results to be ignored
- filters
    literal specifying wanted value for given column
    example:
    ```js
    {
        column: 'value'
    }
    ```
    will return only row for which row.column equal 'value'
    You can pass null as value, and coPostgresQueries, will automatically replace it with IS NULL
    the column name can be appended with the following modifier:
        - not_: != or `IS NOT NULL` if value is null
        - from_: cast to date and compare with >=
        - to_: cast to date and compare with <=
        - like_: ILIKE (match value with case insensitive)
        - not_like_: NOT ILIKE (not match value with case insensitive)

    It is also possible to match to all searchable column with match:

    ```js
        {
            match: 'value',
        }
    ```
    will return only row for which any searchableCols matching value (case insensitive).

- sort:
    Specify the column by which to filter the result (Additionally the result will always get sorted by the row identifiers to avoid random order)
- sortDir:
    Specify the sort direction, either 'ASC' or 'DESC'

#### countAll

```js
import countAll  from 'pg-common-queries/queries/countAll';
countAll({ table, permanentFilters })()
```

Creates a query to count all rows.

##### Configuration

- table: the table name
- permanentFilters: List of filters applied by default, e. g. for a soft delete with permanentFilters as `{ deleted_at: null}`


#### update

```js
import update  from 'pg-common-queries/queries/update';
update({
    table,
    writableCols,
    filterCols,
    returnCols,
    permanentFilters,
})(filters, data);
```

Creates a query to update rows.

##### Configuration

- table: the table name
- writableCols: the columns that can be updated
- filterCols: the columns that can be used to filter the updated rows
- returnCols: the columns to be returned in the result
- permanentFilters: List of filters applied by default, e. g. for a soft delete with permanentFilters as `{ deleted_at: null}`

##### Parameters

Two arguments:

- filters:
    literal specifying wanted value for given column
    example:
    ```js
    {
        column: 'value'
    }
    ```
    will update only row for which column equal 'value'
- data: a literal specifying the new values

#### updateOne

```js
import updateOne  from 'pg-common-queries/queries/updateOne';
updateOne({
    table,
    writableCols,
    primaryKey,
    returnCols,
    permanentFilters,
})(identifier, data);
```

Creates a query to update one row.

##### Configuration

- table: the table name
- writableCols: the columns that can be updated
- primaryKey: One or more columns representing the primary key. Accept either an array or a single value. (default: `id`)
- returnCols: the columns to be returned in the result
- permanentFilters: List of filters applied by default, e. g. for a soft delete with permanentFilters as `{ deleted_at: null}`

##### Parameters

Two arguments:

- identifier: either a single value for a single primaryKey column, or a literal if several columns:`{ id1: value, id2: otherValue }`. All configured primaryKey columns must be given a value.
- data: a literal specifying the column to update

#### remove

```js
import remove  from 'pg-common-queries/queries/remove';
remove({ table, filterCols, returnCols, permanentFilters })(filters);
```

Creates a query to delete rows.

##### Configuration

- table: the table name
- filterCols: the columns that can be used to filter the updated rows
- returnCols: list of columns retrieved by the query
- permanentFilters: List of filters applied by default, e. g. for a soft delete with permanentFilters as `{ deleted_at: null}`

##### Parameters

A literal specifying wanted value for given column
example:

```js
{
    column: 'value'
}
```

will update only row for which column equal 'value'

#### removeOne

```js
import removeOne  from 'pg-common-queries/queries/removeOne';
removeOne({ table, primaryKey, returnCols, permanentFilters })(identitfier);
```

Creates a query to delete one row.

##### Configuration

- table: the table name
- primaryKey: One or more columns representing the primary key. Accept either an array or a single value. (default: `id`)
- returnCols: list of columns retrieved by the query
- permanentFilters: List of filters applied by default, e. g. for a soft delete with permanentFilters as `{ deleted_at: null}`

##### Parameters

The identifier: either a single value for a single primaryKey column, or a literal if several columns:`{ id1: value, id2: otherValue }`. All configured primaryKey columns must be given a value.

#### batchRemove

```js
import batchRemove  from 'pg-common-queries/queries/batchRemove';
batchRemove({ table, primaryKey, returnCols, permanentFilters })(identifierList);
```

Allow to create a query to delete several row at once

##### Configuration

- table: the table name
- columns: list of columns to insert
- primaryKey: One or more columns representing the primary key. Accept either an array or a single value. (default: `id`)
- permanentFilters: List of filters applied by default, e. g. for a soft delete with permanentFilters as `{ deleted_at: null}`

##### Parameters

The list of identifier either an array of single value for a single primaryKey column, or an array of literal if several columns:`[{ id1: value, id2: otherValue }, ...]`. All configured primaryKey columns must be given a value.

#### upsertOne

```js
import upsertOne  from 'pg-common-queries/queries/upsertOne';
upsertOne({
    table,
    primaryKey,
    writableCols,
    returnCols,
    permanentFilters,
})(row)
```

Creates a query to update one row or create it if it does not already exists.

##### Configuration

- table: the name of the table
- primaryKey: One or more columns representing the primary key. Accept either an array or a single value. (default: `id`)
- writableCols: the column that can be updated
- returnCols: the column to return in the result
- permanentFilters: List of filters applied by default, e. g. for a soft delete with permanentFilters as `{ deleted_at: null}`

##### Parameters

the row to upsert

#### batchUpsert

```js
import batchUpsert  from 'pg-common-queries/queries/batchUpsert';
batchUpsert({
    table,
    primaryKey,
    writableCols,
    returnCols,
    permanentFilters,
})(rows)
```

Creates a query to update a batch row creating those that does not already exists.

##### Configuration

- table: the name of the table in which to upsert
- primaryKey: One or more columns representing the primary key. Accept either an array or a single value. (default: `id`)
- writableCols: the column that can be updated
- returnCols: the column to return in the result
- columns: all the columns accepted by the query, default to selectorcolumns + writableCols (no reason to change that)
- permanentFilters: List of filters applied by default, e. g. for a soft delete with permanentFilters as `{ deleted_at: null}`

##### Parameters

The array of rows to upsert

#### selectByOrderedIdentifiers

```js
import selectByOrderedIdentifiers from 'pg-common-queries/queries/selectByOrderedIdentifiers';
selectByOrderedIdentifiers({
    table,
    primaryKey,
    returnCols,
})(values);
```

Creates a query to select multiple row given an array of identifier. The result will keep the order of the identifier. Due to the nature of the query, this will only work for primaryKey composed of a single column.

##### Configuration

- table: the name of the table in which to upsert
- primaryKey: primaryKey of the table (this will only work with primaryKey of a single column)
- returnCols: the column to return in the result

##### Parameters

The array of identifier to retrieve. The array order will determine the result order.

#### crud

```js
import crud  from 'pg-common-queries/queries/crud';
crud({
    table,
    writableCols,
    primaryKey,
    returnCols,
    permanentFilters,
});
```

Creates configured queries for insertOne, batchInsert, selectOne, select, updateOne, deleteOne and batchDelete.

##### Configuration

- table: the name of the table.
- primaryKey: One or more columns representing the primary key. Accept either an array or a single value. (default: `id`)
- writableCols: list of columns that can be set
- returnCols: the list of columns we want returned as result.
- searchableCols: the columns that can be searched (usable in filter parameter). Defaults to return columns
- specificSorts:
    allow to specify sort order for a given column. Useful when we want to order string other than by alphabetical order.
    example:
    ```js
    {
        level: ['master', 'expert', 'novice']
    }
    ```
    will order level column with all master first, then expert and finally novice
- groupByCols: allow to add a GROUP BY clause to the query on the given columns
- withQuery:
    specify that we want to encompass the query in `WITH RESULT AS <query> SELECT * FROM result`
    This add a temporary result table that allow to sort on computed and joined column.
    if the table configuration contain a JOIN clause, this will be automatically set to true.
- permanentFilters: List of filters applied by default, e. g. for a soft delete with permanentFilters as `{ deleted_at: null}`

#### transaction helper

```js
import { begin, commit, savepoint, rollback } from 'pg-common-queries/queries/transaction';
```

Simple helper to manage transaction
You must retrieve a client with `pool.connect()` to use those.

##### begin

```js
import begin from 'pg-common-queries/queries/transaction/begin';
begin();
// { text: 'BEGIN' }
```

create a query to start a transaction

##### commit

```js
import commit from 'pg-common-queries/queries/transaction/commit';
commit();
// { text: 'COMMIT' }
```

create a query to commit a transaction

##### savepoint

```js
import savepoint from 'pg-common-queries/queries/transaction/savepoint';
savepoint(name);
// { text: 'SAVEPOINT name' }
```

create a query to add a save point during transsaction

##### rollback

```js
import rollback from 'pg-common-queries/queries/transaction/rollback';
rollback();
// { text: 'ROLLBACK' }
// or
rollback(name);
// { text: 'ROLLBACK to name' }
```

Rollback the transaction to the given save point, or to its beginning if not specified.